import unittest

from model.address import Address
from model.state import State


class TestAddress(unittest.TestCase):

    def setUp(self):
        self.address = Address(label="home",
                               care_of="Queens Library",
                               street_address="89-11 Merrick Blvd",
                               city="Jamaica",
                               state=State.NEW_YORK,
                               zip="11432")

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
