import unittest
from datetime import datetime

from model.address import Address
from model.person import Person
from model.state import State


class TestPerson(unittest.TestCase):

    def setUp(self):
        self.person = Person(
            name="Abraham Lincoln",
            # assuming UTC is five hours ahead of Kentucky local time
            date_of_birth=datetime(year=1809, month=2, day=12, hour=11, minute=54),
            date_of_death=datetime(year=1865, month=4, day=16, hour=3, minute=15),
            home_address=Address(
                label="home",
                care_of="The White House",
                street_address="1600 Pennsylvania Ave NW",
                city="Washington",
                state=State.DISTRICT_OF_COLUMBIA,
                zip="20500"
            )
        )

    def testPersonDetails(self):
        self.assertEqual(self.person.name, "Abraham Lincoln")
        self.assertEqual(self.person.date_of_birth.year, 1809)
        self.assertEqual(self.person.date_of_birth.month, 2)
        self.assertEqual(self.person.date_of_birth.day, 12)
        self.assertEqual(self.person.home_address.label, "home")

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
