from dataclasses import dataclass
from datetime import datetime

from model.address import Address


@dataclass
class Person:
    name: str
    date_of_birth: datetime
    date_of_death: datetime
    home_address: Address
