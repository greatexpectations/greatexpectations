from dataclasses import dataclass

from model.state import State


@dataclass
class Address:
    label: str
    care_of: str
    street_address: str
    city: str
    state: State
    zip: str
