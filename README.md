By contributing to this repository, you agree to license all your contributions in GPLv3. 

Dear readers,

I intend to add more languages to this project. 
I welcome all contributions. 
Please feel free to fork this repo and use it to learn. 

Sincerely, 
